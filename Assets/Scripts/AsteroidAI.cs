using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidAI : MonoBehaviour
{
    public int damage = 1;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Base")
        {
            GameManager gm = FindObjectOfType<GameManager>();
            Health planetHP = collision.GetComponent<Health>();
            if (planetHP != null)
            {
                planetHP.TakeDamage(damage);
            }

            Destroy(this.gameObject);
            gm.CheckAsteroids();
        }
    }
}
