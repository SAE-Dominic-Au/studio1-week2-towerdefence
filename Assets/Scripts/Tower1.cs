using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower1 : MonoBehaviour
{
    public float range;
    public float cost;
    public float damage;

    public LineRenderer LR;
    public List<GameObject> asteroidsToShootAt;

    // Start is called before the first frame update
    void Start()
    {
        transform.GetChild(0).localScale = new Vector3(range, range, 1);

        Vector4 newColor = transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().color;
        newColor.w = 0;
        transform.GetChild(0).GetComponent<SpriteRenderer>().color = newColor;

        LR = this.GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
        if (asteroidsToShootAt.Count > 0)
        {
            if(LR != null)
            {
                LR.positionCount = 2;
                LR.SetPosition(0, transform.position);
                LR.SetPosition(1, asteroidsToShootAt[0].transform.position);

                Health hp = asteroidsToShootAt[0].GetComponent<Health>();
                if(hp != null)
                {
                    hp.TakeDamage(damage * Time.deltaTime);
                }
            }
        }
        else
        {
            LR.positionCount = 1;
            LR.SetPosition(0, transform.position);
        }

        asteroidsToShootAt = this.GetComponentInChildren<TowerDetection>().asteroidsToShootAt;
    }
}
