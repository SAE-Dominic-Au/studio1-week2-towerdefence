using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Crosshair : MonoBehaviour
{
    public int collidersTouched = 0;
    public bool isEmpty = true;

    public bool placing;
    public bool selecting;
    public bool deleting;

    public Text towerText;
    public Text controlsText;

    public GameObject tower1;
    public GameObject tower2;
    public GameObject tower3;

    public GameObject spawnTower;
    public GameObject selectedTower;

    public GameManager gameManager;

    public string[] tagCollection = new string[] { "Player", "Env" };
    // Start is called before the first frame update
    void Start()
    {
        PlaceButton();
        Tower3Button();

        if (gameManager == null)
        {
            gameManager = FindObjectOfType<GameManager>();  
        }
    }

    // Update is called once per frame
    void Update()
    {
        //isEmpty = true;
        if(Input.GetMouseButtonDown(0))
        {
            Vector3 mousePos = Input.mousePosition;
            if(transform.position.x >= -14 && transform.position.x <= 14 && transform.position.y >= -8 && transform.position.y <= 4)
            {
                if(placing == true)
                {
                    if (spawnTower != null && gameManager.currentTower < gameManager.towerCap && isEmpty == true)
                    {
                        Tower1 t1 = spawnTower.GetComponent<Tower1>();
                        Tower2 t2 = spawnTower.GetComponent<Tower2>();
                        Tower3 t3 = spawnTower.GetComponent<Tower3>();
                        if (t1 != null)
                        {
                            float cost = t1.cost;
                            if (gameManager.currentGold >= cost)
                            {
                                Instantiate(spawnTower, transform.position, Quaternion.identity);
                                gameManager.currentGold -= cost;
                            }
                        }
                        else if (t2 != null)
                        {
                            float cost = t2.cost;
                            if (gameManager.currentGold >= cost)
                            {
                                Instantiate(spawnTower, transform.position, Quaternion.identity);
                                gameManager.currentGold -= cost;
                            }
                        }
                        else
                        {
                            float cost = t3.cost;
                            if (gameManager.currentGold >= cost)
                            {
                                Instantiate(spawnTower, transform.position, Quaternion.identity);
                                gameManager.currentGold -= cost;
                            }
                        }
                    }
                }

                if(selecting == true)
                {
                    Vector4 newColor = selectedTower.transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().color;
                    newColor.w = 0.25f;
                    selectedTower.transform.GetChild(0).GetComponent<SpriteRenderer>().color = newColor;
                }

                if (deleting == true)
                {
                    Destroy(selectedTower);
                }

                gameManager.CheckTowers();
                gameManager.UpdateTextUI();
            }
            else
            {
                Debug.Log("Mouse is out of bounds");
            }
        }

        isEmpty = false;
        if(collidersTouched == 0)
        {
            isEmpty = true;
        }

        Vector3 newPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        newPosition.z = 0;
        transform.position = newPosition;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(CheckTag(tagCollection, collision.tag))
        {
            collidersTouched += 1;

            if(collision.tag == "Player")
            {
                selectedTower = collision.gameObject;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (CheckTag(tagCollection, collision.tag))
        {
            collidersTouched -= 1;

            if (collision.tag == "Player")
            {
                if (selectedTower == collision.gameObject)
                {
                    Vector4 newColor = selectedTower.transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().color;
                    newColor.w = 0;
                    selectedTower.transform.GetChild(0).GetComponent<SpriteRenderer>().color = newColor;
                }

                selectedTower = null;
            }
        }
    }

    private bool CheckTag(string[] tagCollection, string tagToCheck)
    {
        bool tagExists = false;

        for(int i= 0; i < tagCollection.Length; i++)
        {
            if(tagCollection[i] == tagToCheck)
            {
                tagExists = true;
            }
        }
        return tagExists;
    }

    public void PlaceButton()
    {
        placing = true;
        selecting = false;
        deleting = false;

        controlsText.text = "Controls: Placing";
    }

    public void SelectButton()
    {
        placing = false;
        selecting = true;
        deleting = false;

        controlsText.text = "Controls: Selecting";
    }

    public void DeleteButton()
    {
        placing = false;
        selecting = false;
        deleting = true;

        controlsText.text = "Controls: Deleting";
    }

    public void Tower1Button()
    {
        spawnTower = tower1;
        towerText.text = "Towers: Tower 1";
    }

    public void Tower2Button()
    {
        spawnTower = tower2;
        towerText.text = "Towers: Tower 2";
    }

    public void Tower3Button()
    {
        spawnTower = tower3;
        towerText.text = "Towers: Tower 3";
    }
}
