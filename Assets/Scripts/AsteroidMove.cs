using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidMove : MonoBehaviour
{
    public GameObject path;
    public Vector2[] paths;
    public GameObject offset;
    public int checkpoint;

    public Vector2 targetPos;
    public float speed;
    public float speedModifier;
    public float duration;
    // Start is called before the first frame update
    void Start()
    {
        checkpoint = 1;
        paths = new Vector2[path.transform.childCount];
        for (int i = 0; i < path.transform.childCount;i++)
        {
            paths[i] = path.transform.GetChild(i).position;
            paths[i] += new Vector2(offset.transform.position.x, offset.transform.position.y);
        }

        transform.position = paths[0];
        targetPos = paths[checkpoint];
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position, targetPos, speed * speedModifier * Time.deltaTime);

        if(Vector2.Distance(transform.position, targetPos) < 0.1f)
        {
            checkpoint++;
            if(checkpoint< path.transform.childCount)
            {
                targetPos = paths[checkpoint];
            }
        }

        if(duration > 0)
        {
            duration -= Time.deltaTime;

            if(duration <= 0)
            {
                speedModifier = 1;
            }
        }
    }
}
