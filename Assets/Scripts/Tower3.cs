using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower3 : MonoBehaviour
{
    public GameObject playerProjectile3;
    public Transform spawnLocation;
    public float range;
    public float cost;
    public float fireRate;
    private float counter;

    public List<GameObject> asteroidsToShootAt;

    // Start is called before the first frame update
    void Start()
    {
        transform.GetChild(0).localScale = new Vector3(range, range, 1);


        Vector4 newColor = transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().color;
        newColor.w = 0;
        transform.GetChild(0).GetComponent<SpriteRenderer>().color = newColor;
    }

    // Update is called once per frame
    void Update()
    {
        if(counter > 0)
        {
            counter -= Time.deltaTime;
        }

        if(counter <= 0)
        {
            if(asteroidsToShootAt.Count != 0)
            {
                GameObject temp = Instantiate(playerProjectile3, spawnLocation.position, Quaternion.identity);
                GameManager gm = FindObjectOfType<GameManager>();
                gm.PlayClip(gm.pew);


                PlayerProjectile3 playerProj = temp.GetComponent<PlayerProjectile3>();
                if (playerProj != null)
                {
                    playerProj.targetToHome = asteroidsToShootAt[0];
                }
                
                for(int i = 0; i < asteroidsToShootAt.Count; i++)
                {
                    
                }

                counter = fireRate;
            }
        }

        asteroidsToShootAt = this.GetComponentInChildren<TowerDetection>().asteroidsToShootAt;
    }
}
