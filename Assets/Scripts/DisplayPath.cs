using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayPath : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        int count = transform.childCount;
        this.GetComponent<LineRenderer>().positionCount = count;

        for (int i = 0; i < count; i++)
        {
            this.GetComponent<LineRenderer>().SetPosition(i, transform.GetChild(i).position);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
