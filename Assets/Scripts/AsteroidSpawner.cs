using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidSpawner : MonoBehaviour
{

    public int spawnPerWave = 5;
    public int level = 1;
    public int spawnedCounter = 0;
    public float spawnRate = 0.2f;
    public Transform spawnLocation;
    public Vector3 offset;
    public GameObject asteroidPrefab;

    private float counter;
    // Start is called before the first frame update
    void Start()
    {
        spawnedCounter = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if(counter > 0)
        {
            counter -= Time.deltaTime;
        }

        if (counter <= 0 && spawnedCounter < (spawnPerWave * level))
        {
            Instantiate(asteroidPrefab);
            spawnedCounter++;
            counter = spawnRate;            
            FindObjectOfType<GameManager>().allSpawned = true;
            FindObjectOfType<GameManager>().CheckAsteroids();
        }
    }
}
