using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public bool canGenerateGold = true;
    public float goldIncome;
    public float currentGold;
    public float defaultGold = 100;

    public int currentTower;
    public int towerCap;
    public int defaultTowerCap = 5;

    public Health planetHP;
    public GameObject planetPrefab;

    public Button levelCompleteButton;
    public Button levelRestartButton;

    public Text goldText;
    public Text towerText;
    public Text planetHPText;
    public Text levelText;

    public int level = 1;
    public AsteroidSpawner aS;

    public int sceneIndex;
    public int endSceneIndex;

    public bool allSpawned = false;
    private int asteroidCount = 0;

    public AudioClip pew;
    public AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        currentGold = defaultGold * level;
        towerCap = defaultTowerCap * level;
        aS.level = level;
        levelCompleteButton.gameObject.SetActive(false);
        levelRestartButton.gameObject.SetActive(false);
        CheckTowers();
        UpdateTextUI();

        sceneIndex = SceneManager.GetActiveScene().buildIndex;
        endSceneIndex = SceneManager.sceneCountInBuildSettings;
    }

    // Update is called once per frame
    void Update()
    {
        if(canGenerateGold == true)
        {
            currentGold += goldIncome * Time.deltaTime;
            UpdateTextUI();
        }

        CheckAsteroids();
    }

    public void CheckTowers()
    {
        currentTower = GameObject.FindGameObjectsWithTag("Player").Length;
    }

    public void UpdateTextUI()
    {
        goldText.text = "$" + Mathf.Round(currentGold).ToString();
        towerText.text = currentTower.ToString() + " / " + towerCap.ToString();
        planetHPText.text = planetHP.currentHP.ToString() + " / " + planetHP.maxHP.ToString();
        levelText.text = level.ToString();
    }

    public void CheckPlanet(float currentHP)
    {
        if (currentHP <= 0)
        {
            canGenerateGold = false;
            levelRestartButton.gameObject.SetActive(true);
        }
    }

    public void CheckAsteroids()
    {
        asteroidCount = FindObjectsOfType<AsteroidAI>().Length;

        if (asteroidCount == 0 && allSpawned == true && planetHP != null)
        {
            canGenerateGold = false;
            levelCompleteButton.gameObject.SetActive(true);
        }
    }

    public void levelComplete()
    {
        level += 1;

        currentGold = defaultGold * level;
        currentTower = 0;
        towerCap = defaultTowerCap * level;
        aS.level = level;
        allSpawned = false;

        planetHP.currentHP = planetHP.maxHP;

        levelCompleteButton.gameObject.SetActive(false);
        levelRestartButton.gameObject.SetActive(false);


        Tower1[] allTower1 = FindObjectsOfType<Tower1>();
        Tower2[] allTower2 = FindObjectsOfType<Tower2>();
        Tower3[] allTower3 = FindObjectsOfType<Tower3>();

        foreach(Tower1 tower in allTower1)
        {
            Destroy(tower.gameObject);
        }

        foreach (Tower2 tower in allTower2)
        {
            Destroy(tower.gameObject);
        }

        foreach (Tower3 tower in allTower3)
        {
            Destroy(tower.gameObject);
        }

        aS.spawnedCounter = 0;
        UpdateTextUI();
    }

    public void levelRestart()
    {
        currentGold = defaultGold * level;
        currentTower = 0;
        towerCap = defaultTowerCap * level;
        aS.level = level;
        allSpawned = false;
        levelCompleteButton.gameObject.SetActive(false);
        levelRestartButton.gameObject.SetActive(false);

        GameObject planet = Instantiate(planetPrefab, planetPrefab.transform.position, Quaternion.identity);
        planetHP = planet.GetComponent<Health>();

        Tower1[] allTower1 = FindObjectsOfType<Tower1>();
        Tower2[] allTower2 = FindObjectsOfType<Tower2>();
        Tower3[] allTower3 = FindObjectsOfType<Tower3>();

        foreach (Tower1 tower in allTower1)
        {
            Destroy(tower.gameObject);
        }

        foreach (Tower2 tower in allTower2)
        {
            Destroy(tower.gameObject);
        }

        foreach (Tower3 tower in allTower3)
        {
            Destroy(tower.gameObject);
        }

        aS.spawnedCounter = 0;
        UpdateTextUI();
    }

    public void PlayClip(AudioClip audioClip)
    {
        audioSource.PlayOneShot(audioClip);
    }
}
