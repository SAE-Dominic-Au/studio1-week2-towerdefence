using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public float currentHP;
    public float maxHP = 5;

    // Start is called before the first frame update
    void Start()
    {
        currentHP = maxHP;
    }

    public void TakeDamage(float damage)
    {
        currentHP -= damage;
        GameManager gm = FindObjectOfType<GameManager>();
        if (currentHP <= 0)
        {
            
            if (this.gameObject.tag == "Enemy")
            {
                gm.currentGold += 10;
                Destroy(this.gameObject);
            }
            else if (this.gameObject.tag == "Base")
            {
                
                gm.CheckPlanet(currentHP);
                Destroy(this.gameObject);
            }
        }
    }
}
