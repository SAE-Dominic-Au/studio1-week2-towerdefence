using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerDetection : MonoBehaviour
{

    public List<GameObject> asteroidsToShootAt;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //SortList();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Enemy")
        {
            asteroidsToShootAt.Add(collision.gameObject);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Enemy")
        {
            asteroidsToShootAt.Remove(collision.gameObject);
        }
    }

    /*
    public void SortList()
    {
        List<GameObject> newListA = asteroidsToShootAt;
        List<GameObject> empty = new List<GameObject>();
        for (int a = 0; a < newListA.Count; a++)
        {
            float maxX = newListA[a].transform.position.x;
            int index = a;
            for (int b = 1; b < newListA.Count; b++)
            {
                if (newListA[b].transform.position.x > maxX)
                {
                    maxX = newListA[b].transform.position.x;
                    index = b;
                }

                if (b == (newListA.Count - 1))
                {
                    empty.Add(newListA[index]);
                    newListA.RemoveAt(index);
                }
            }
        }

        asteroidsToShootAt = empty;
    }
    */
}
