using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerProjectile3 : MonoBehaviour
{
    public GameObject targetToHome;
    public Vector2 lastKnownPosition;
    public float speed = 10;
    public float damage = 2;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(targetToHome != null)
        { 
            transform.position = Vector2.MoveTowards(transform.position, targetToHome.transform.position, speed * Time.deltaTime);
            lastKnownPosition = targetToHome.transform.position;
        }
        else
        {
            transform.position = Vector2.MoveTowards(transform.position, lastKnownPosition, speed * Time.deltaTime);
            if(Vector2.Distance(transform.position, lastKnownPosition) < 0.1f)
            {
                Destroy(this.gameObject);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Enemy")
        {
            Health hp = collision.GetComponent<Health>();

            if(hp != null)
            {
                hp.TakeDamage(damage);
            }
            Destroy(this.gameObject);
        }
    }
}
